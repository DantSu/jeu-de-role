<?php

namespace Controllers;

function callView(string $path_view, array $array_variables): string
{
    extract($array_variables);

    ob_start();
    include PATH_RESOURCES.'views/' . $path_view . '.php';
    $str = ob_get_contents();
    ob_end_clean();
    return $str;
}

abstract class Action
{

    protected $controller = null;
    protected $file = null;
    protected $vars = [];

    public function __construct(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Vue render
     *
     * @param string $path_view String which define the path to the view
     * @param array $array_variables Array which define the variables you want to get in your view.
     * @return Action
     */
    public function setView(string $path_view, array $array_variables=[]): Action
    {
        $this->file = $path_view;
        $this->vars = $array_variables;
        return $this;
    }

    /**
     * Vue render
     *
     * @return string
     */
    public function render(): string
    {
        if (!$this->file)
            return '';

        return callView($this->file, $this->vars);
    }

}