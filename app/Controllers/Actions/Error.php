<?php

namespace Controllers\Actions;

use Controllers\Action;

class Error extends Action {
    public function notFound(): Action {
        return (new Layout($this->controller))
            ->setTitle('Page non trouvée')
            ->getLayout($this->setView('404')->render());
    }
}