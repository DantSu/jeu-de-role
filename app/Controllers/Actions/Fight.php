<?php

namespace Controllers\Actions;

use Controllers\Action;
use Models\Characters\Character;
use Models\Characters\Goblin;

class Fight extends Action {
    public function show(): Action {

        /**
         * @var $heros Character[]
         * @var $monsters Character[]
         * @var $monster Character | Goblin
         * @var $size_attacker int
         * @var $is_hero_turn bool
         * @var $has_error bool
         * @var $nb_goblins int
         */

        $heros = [];
        $monsters = [];
        $size_attacker = 0;
        $has_error = false;
        $nb_goblins = 0;
        $heros_died = 0;
        $monsters_died = 0;


        if ($this->controller->request->getPost('submit') != null) {
            /* Get goblins */
            $count_values = $this->controller->request->postCountValues();

            if (isset($count_values['Goblin'])) {
                $nb_goblins = $count_values['Goblin'];
            }

            for ($i=1;$i<4;$i++) {
                $hero_name = $this->controller->request->getPost('hero-' . $i . '-name');
                $hero_type = $this->controller->request->getPost('hero-' . $i . '-type');
                if ($hero_name && $hero_type) {
                    $class_name = 'Models\\Characters\\'.$hero_type;
                    $heros[] = new $class_name($hero_name);
                }

                $monster_name = $this->controller->request->getPost('monster-' . $i . '-name');
                $monster_type = $this->controller->request->getPost('monster-' . $i . '-type');
                if ($monster_name && $monster_type) {
                    $class_name = 'Models\\Characters\\'.$monster_type;
                    $monster = new $class_name($monster_name);

                    if ($monster instanceof Goblin)
                        $monster->setNumber($nb_goblins);

                    $monsters[] = $monster;
                }
            }
        } else {
            $has_error = true;
        }


        /**
         * Let's fight begin !!!!!!
         */

        if ($has_error) {
            $html_fight_content = $this->setView('fight/error')->render();
        } else {
            $html_fight_steps = '';

            while ( ($heros_died != 3) && ($monsters_died != 3) ) {
                $size_attacker++;
                $is_hero_turn = (bool) ($size_attacker%2);

                if ($is_hero_turn) {
                    $size = 'heros';
                    $attacker = $heros[$heros_died];
                    $defender = $monsters[$monsters_died];
                } else {
                    $size = 'monsters';
                    $attacker = $monsters[$monsters_died];
                    $defender = $heros[$heros_died];
                }

                /* Attack */
                $attacker->hit($defender, $heros_died, $monsters_died);

                if ($defender->getHealth() == 0) {
                    if ($is_hero_turn)
                        $monsters_died++;
                    else
                        $heros_died++;
                }

                $characters = $heros;
                $html_status_heros = $this->setView('fight/fight-status', compact('characters', 'size', 'size_attacker'))->render();

                $characters = $monsters;
                $html_status_monsters = $this->setView('fight/fight-status', compact('characters', 'size', 'size_attacker'))->render();

                $html_fight_steps .= $this->setView('fight/fight-step', compact('size_attacker', 'attacker', 'defender', 'html_status_heros', 'html_status_monsters'))->render();
            }
            
            if ($monsters_died > $heros_died) {
                $html_fight_result = $this->setView('fight/fight-win')->render();
            } else {
                $html_fight_result = $this->setView('fight/fight-loose')->render();
            }
            
            $html_fight_content = $this->setView('fight/fight-layout', compact('html_fight_steps', 'html_fight_result'))->render();
        }

        return (new Layout($this->controller))
            ->setTitle('Déroulement du combat')
            ->getLayout(
                $this
                    ->setView(
                        'fight/layout',
                        compact('html_fight_content')
                    )
                    ->render()
            );
    }
}