<?php

namespace Controllers\Actions;

use Controllers\Action;

class Home extends Action {
    public function show(): Action {
        return (new Layout($this->controller))->getLayout(
            $this
                ->setView('home')
                ->render()
        );
    }
}