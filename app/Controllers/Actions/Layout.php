<?php

namespace Controllers\Actions;

use Controllers\Action;

class Layout extends Action {
    private $title = '';

    public function setTitle(string $title): Layout {
        $this->title = $title;
        return $this;
    }

    public function getLayout(string $content_html): Action {
        return $this->setView('layout', ['content_html'=>$content_html,'title'=>$this->title]);
    }
}