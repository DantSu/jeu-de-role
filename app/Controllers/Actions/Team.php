<?php

namespace Controllers\Actions;

use Controllers\Action;
use Models\Datas;

class Team extends Action {
    public function show(): Action {
        return (new Layout($this->controller))
            ->setTitle('Création d\'une équipe')
            ->getLayout(
                $this
                    ->setView(
                        'teams',
                        ['characters' => Datas\Characters::CHARACTERS]
                    )
                    ->render()
            );
    }
}