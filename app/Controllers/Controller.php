<?php 
namespace Controllers;

/**
 * Controller for return view
 * 
 */
class Controller
{
    public $request;
    public $response;

    public function __construct()
    {
        $this->request = new Request($this);
        $this->response = new Response();
    }

    /**
     * Execute the program
     */
    public function execute()
    {
        $class_action_requested  = $this->request->getAction();
        $method_action_requested = $this->request->getMethod();

        $action_requested = new $class_action_requested($this);

        $this->response
            ->setHTML($action_requested->$method_action_requested()->render())
            ->printOut();
    }
}