<?php

namespace Controllers;

class Request
{

    private $action = null;
    private $method = null;
    private $post;

    public function __construct(Controller $controller)
    {
        $routes = include PATH_ROUTES.'web.php';

        $this
            ->setAction(null)
            ->setMethod(null);

        foreach ($routes as $pattern => $callable) {
            if(preg_match('~'.$pattern.'~i', $_SERVER['REQUEST_URI'], $matches)) {
                //var_dump($pattern, $_SERVER['REQUEST_URI'], $matches);
                //die();
                $callable($this, array_slice($matches, 1));
            }
        }

        if(is_null($this->action) || is_null($this->method))
            $this
                ->setAction('Controllers\Actions\Error')
                ->setMethod('notFound');

        $this->post = $_POST;
    }


    public function setAction($action): Request
    {
        $this->action = $action;
        return $this;
    }

    public function setMethod($method): Request
    {
        $this->method = $method;
        return $this;
    }
    public function getAction(): string
    {
        return $this->action;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getPost($key, $default=null) {
        if(isset($this->post[$key]))
            return $this->post[$key];

        return $default;
    }
    public function postCountValues(): array {
        return array_count_values($this->post);
    }
}