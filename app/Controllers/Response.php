<?php
namespace Controllers;

class Response {
    private $html = '';

    public function setHTML(string $html): Response {
        $this->html = $html;
        return $this;
    }

    public function printOut() {
        echo $this->html;
        die();
    }

}