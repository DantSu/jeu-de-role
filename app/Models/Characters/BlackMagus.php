<?php

namespace Models\Characters;

/**
 * Create class Black magus as Character's class extend
 * 
 * Speciality : 
 * 
 * - speciality attack : Wizard (ID = 1)
 * - health : 400
 * - Strength : 100 to 200
 * - Side : dark
 */

class BlackMagus extends Character
{
    const TYPE = 'Mage noir';
    const TYPE_ID = 5;
    const SPECIALITY_ID = 1;
    
    public function __construct($name)
    {
        $this->_name = $name;
        $this->_health = 400;
        $this->_strength_min = 100;
        $this->_strength_max = 200;
    }
}