<?php

namespace Models\Characters;

/**
 * Create Class black shadow as Character's class extend
 * 
 * Speciality : 
 * 
 * - speciality attack : Elf (ID = 2)
 * - health : 350
 * - Strength : 150 to 250
 * - Side : dark
 */

class BlackShadow extends Character
{
    const TYPE = 'Ombre noire';
    const TYPE_ID = 8;
    const SPECIALITY_ID = 2;
    
    public function __construct($name)
    {
        $this->_name = $name;
        $this->_health = 350;
        $this->_strength_min = 150;
        $this->_strength_max = 250;
    }
}