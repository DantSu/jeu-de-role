<?php

namespace Models\Characters;

/**
 * Create class Character
 * 
 * Parent class used for all character. Contain hit and all getter methods
 */

abstract class Character
{
    const TYPE = '';
    const TYPE_ID = 0;
    const SPECIALITY_ID = 0;

    protected $_name;
    protected $_health;
    protected $_strength_min;
    protected $_strength_max;
    protected $_level = 1;
    protected $_last_dammage = 0; /* Use for keep the value of last hit made by an attacker */

    abstract public function __construct($name);

    /**
     * Hit method, call when an attacker hit a defender
     *
     * @param int $heros_died Number of heros died
     * @param int $monsters_died Number of monsters died
     * @param Character $defender. Object ref of the defender
     * 
     * @return void
     */
    public function hit(Character $defender, int $heros_died,  int $monsters_died)
    {
        /* Get dammage */
        $this->_last_dammage = rand($this->_strength_min, $this->_strength_max);

        /* Extra bonus added on dammage */
        if (method_exists($this, 'additionnal_attack_bonus')) {
            if($this->additionnal_attack_bonus($heros_died, $monsters_died)) {
                $this->_last_dammage *= 1.5;
            }
        }

        /* bonus if attack him speciality */
        if ($this::SPECIALITY_ID && ($this::SPECIALITY_ID == $defender::TYPE_ID)) {
            $this->_last_dammage *= 1.5;
        }

        /* Set dammage on defender and level up attacker if defender is dead */
        if (!$defender->hurt($this->_last_dammage)) {
            $this->_level++;
            $this->_strength_min += 50;
            $this->_strength_max += 50;
        }
    }
    
    /**
     * Hurt method, call when a defender was attacked by an attacker
     * 
     * @param int $dammage. Dammage of the attack
     * 
     * @return bool $health defender
     */
    public function hurt(int $dammage): bool
    {
        /* Set dammage */
        $this->_health = max($this->_health - $dammage, 0);
       
        /* Extra bonus added on defender */
        if (method_exists($this, 'additionnal_defense_bonus')) {
            $this->additionnal_defense_bonus();
        }

        return ($this->_health > 0);
    }
    
    /**
     * Get level method
     * 
     * @return int $level character
     */
    public function getLevel(): int
    {
        return $this->_level;
    }

     /**
     * Get health method. Health = 0 > character is died
     * 
     * @return int $health character
     */
    public function getHealth(): int
    {
        return $this->_health;
    }

    /**
     * Get name method
     * 
     * @return string $name character
     */
    public function getName(): string
    {
        return $this->_name;
    }

    /**
     * Get last dammage method
     * 
     * @return int $lastDammage character
     */
    public function getDammage(): int
    {
        return $this->_last_dammage;
    }
}