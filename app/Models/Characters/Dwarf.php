<?php

namespace Models\Characters;

/**
 * Create Class Dwarf as Character's class extend
 * 
 * Speciality : 
 * 
 * - speciality attack : Goblin (ID = 7)
 * - health : 450
 * - Strength : 100 to 150
 * - Side : light
 */
class Dwarf extends Character
{
    const TYPE = 'Nain';
    const TYPE_ID = 3;
    const SPECIALITY_ID = 7;
    
    public function __construct ($name) {
        $this->_name = $name;
        $this->_health = 450;
        $this->_strength_min = 100;
        $this->_strength_max = 150;
    }
}