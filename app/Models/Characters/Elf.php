<?php

namespace Models\Characters;

/**
 * Create Class Elf as Character's class extend
 * 
 * Speciality : 
 * 
 * - speciality attack : Orc (ID = 6)
 * - health : 500
 * - Strength : 0 to 300
 * - Side : light
 */
class Elf extends Character
{
    const TYPE = 'Elfe';
    const TYPE_ID = 2;
    const SPECIALITY_ID = 6;
    
    public function __construct($name)
    {
        $this->_name = $name;
        $this->_health = 500;
        $this->_strength_min = 0;
        $this->_strength_max = 300;
    }
}