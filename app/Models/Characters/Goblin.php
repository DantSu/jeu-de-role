<?php

namespace Models\Characters;

/**
 * Create Class Goblin as Character's class extend
 * 
 * Speciality : 
 * 
 * - speciality attack : Warrior (ID = 4)
 * - health : 250
 * - Strength : 50 to 200
 * - Side : dark
 * - If there is 2 or more Goblin in the team, they have a double strength extremity values
 */
class Goblin extends Character
{
    const TYPE = 'Gobelin';
    const TYPE_ID = 7;
    const SPECIALITY_ID = 4;
    
    public function __construct($name)
    {
        $this->_name = $name;
        $this->_health = 250;
        $this->setNumber(1);
    }

    public function setNumber($number_goblins) {
        $this->_strength_min = $number_goblins > 2 ? 100 : 50;
        $this->_strength_max = $number_goblins > 2 ? 400 : 200;
    }
}