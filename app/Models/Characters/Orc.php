<?php

namespace Models\Characters;

/**
 * Create Class Orc as Character's class extend
 * 
 * Speciality : 
 * 
 * - speciality attack : Dwarf (ID = 3)
 * - health : 400
 * - Strength : 200 to 250
 * - Side : dark
 */
class Orc extends Character
{
    const TYPE = 'Orque';
    const TYPE_ID = 6;
    const SPECIALITY_ID = 3;
    
    public function __construct($name)
    {
        $this->_name = $name;
        $this->_health = 400;
        $this->_strength_min = 200;
        $this->_strength_max = 250;
    }
}