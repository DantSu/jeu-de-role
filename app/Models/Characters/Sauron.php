<?php

namespace Models\Characters;

/**
 * Create Class Orc as Character's class extend
 * 
 * Speciality : 
 * 
 * - speciality attack : None
 * - health : 1000
 * - Strength : 200 to 400
 * - Side : dark
 * - have an attack bonus when he is alone
 */
class Sauron extends Character
{
    const TYPE = 'Sauron';
    const TYPE_ID = 9;
    const SPECIALITY_ID = 0;
    
    public function __construct($name)
    {
        $this->_name = $name;
        $this->_health = 1000;
        $this->_strength_min = 200;
        $this->_strength_max = 400;
    }
    /**
     * Add additionnal attack bonus when character is alone against 2 heroes
     *
     * @param int $heros_died Number of heros died
     * @param int $monsters_died Number of monsters died
     * @return bool Is additionnal attack bonus enabled
     */
    public function additionnal_attack_bonus(int $heros_died, int $monsters_died)
    {
        return ($heros_died < 1) && ($monsters_died == 2);
    }
}