<?php

namespace Models\Characters;

/**
 * Create Class Warrior as Character's class extend
 * 
 * Speciality : 
 * 
 * - speciality attack : Black shadow (ID = 8)
 * - health : 400
 * - Strength : 150 to 200
 * - Side : light
 */
class Warrior extends Character
{
    const TYPE = 'Guerrier';
    const TYPE_ID = 4;
    const SPECIALITY_ID = 8;
    
    public function __construct($name)
    {
        $this->_name = $name;
        $this->_health = 400;
        $this->_strength_min = 150;
        $this->_strength_max = 200;
    }
}