<?php

namespace Models\Characters;

/**
 * Create Class Wizard as Character's class extend
 * 
 * Speciality : 
 * 
 * - speciality attack : Dwarf (ID = 3)
 * - health : 400
 * - Strength : 100 to 200
 * - Side : light
 * - Have an extract bonus when he is almost died (health < 100)
 */
class Wizard extends Character
{
    const TYPE = 'Magicien';
    const TYPE_ID = 1;
    const SPECIALITY_ID = 5;
    
    public function __construct($name)
    {
        $this->_name = $name;
        $this->_health = 400;
        $this->_strength_min = 100;
        $this->_strength_max = 200;
    }
    /**
     * Additionnal bonus when the caracter is almost died
     */
    public function additionnal_defense_bonus()
    {
        if($this->_health < 100) {
            $this->_strength_max = 300;
        }
    }
}