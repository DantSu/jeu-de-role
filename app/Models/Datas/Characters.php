<?php

namespace Models\Datas;

class Characters
{
    const CHARACTERS = [
        'hero' =>
            [
                'singular' => 'Héro',
                'plurial'  => 'Héros',
                'types'    =>
                    [
                        'Wizard'  => 'Magicien',
                        'Elf'     => 'Elfe',
                        'Dwarf'   => 'Nain',
                        'Warrior' => 'Guerrier'
                    ]
            ],
        'monster' =>
            [
                'singular' => 'Monstre',
                'plurial'  => 'Monstres',
                'types'    =>
                    [
                        'BlackMagus'  => 'Mage noir',
                        'Orc'         => 'Orque',
                        'Goblin'      => 'Gobelin',
                        'BlackShadow' => 'Ombre noire',
                        'Sauron'      => 'Sauron'
                    ]
            ]
    ];
}
