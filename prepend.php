<?php
session_start();

define('PATH_APP',             __DIR__.'/app/');
define('PATH_PUBLIC',          __DIR__.'/public/');
define('PATH_RESOURCES',       __DIR__.'/resources/');
define('PATH_RESOURCES_VIEWS', PATH_RESOURCES.'views/');
define('PATH_ROUTES',          __DIR__.'/routes/');

spl_autoload_register(function($class) {
    require_once PATH_APP.str_replace('\\', '/', $class).'.php';
});