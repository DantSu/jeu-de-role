(function($) {
    $('.js-toggle').each(function() {
        var $w = $(this),
            $btn = $('[aria-controls]', $w),
            $c = $('[aria-expanded]', $w);

        $btn.click(function() {
            $w.toggleClass('open');
            if($c.attr('aria-expanded')) {
                $c.attr('aria-expanded', 'false');
            } else {
                c.attr('aria-expanded', 'true');
            }
            
        });
    });
})(jQuery);