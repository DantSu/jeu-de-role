<main class="site-main home">
    <div class="home-wrapper">
        <h1 class="home-title">La page que vous cherchez n'existe pas :/</h1>
        <a href="/" class="btn">Retourner sur la page d'accueil</a>
    </div>
</main>