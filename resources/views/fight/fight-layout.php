        <section class="site-section">
            <h2 class="site-title">Déroulement du combat</h2>
                <?= $html_fight_steps ?>
            </section>

            <section class="site-section">
                <h2 class="site-title">Fin du combat</h2>
                <?= $html_fight_result ?>
            </section>