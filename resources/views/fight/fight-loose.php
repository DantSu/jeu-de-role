                <div class="defeat">
                    <h3>Défaite !</h3>
                    <p>
                        - Qu'allez-vous faire, alors? demanda Pippin, sans se laisser démonter par les sourcils hérissés du magicien.<br>
                        - Cognez sur les portes avec votre tête, Peregrin Touque, dit Gandalf. Mais si cela ne les fracasse pas et qu'on me libère un peu des questions stupides, je chercherai à trouver la formule d'ouverture.
                    </p>
                </div>