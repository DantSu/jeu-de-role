        <table class="list-characters">
            <tr>
                <th id="<?= $size ?>-name-<?= $size_attacker ?>" scope="col">Nom</th>
                <th id="<?= $size ?>-type-<?= $size_attacker ?>" scope="col">Type</th>
                <th id="<?= $size ?>-level-<?= $size_attacker ?>" scope="col">Niveau</th>
                <th id="<?= $size ?>-health-<?= $size_attacker ?>" scope="col">Vie</th>
            </tr>

            <?php foreach($characters as $caracter): ?>
            <tr>
                <td headers="<?= $size ?>-name-<?= $size_attacker ?>"><?= $caracter->getName() ?></td>
                <td headers="<?= $size ?>-type-<?= $size_attacker ?>"><?= $caracter::TYPE ?></td>
                <td headers="<?= $size ?>-level-<?= $size_attacker ?>"><?= $caracter->getLevel() ?></td>
                <td headers="<?= $size ?>-health-<?= $size_attacker ?>"><?= $caracter->getHealth() ?></td>
            </tr>
            <?php endforeach; ?>
            
        </table>