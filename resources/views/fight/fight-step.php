                <div class="action js-toggle">
                    <button class="action-btn" aria-controls="action-content-<?= $size_attacker ?>" id="action-btn-<?= $size_attacker ?>">
                        <span class="action-id">Action <?= $size_attacker ?></span>
                        <strong><?= $attacker->getName() ?></strong> attaque <strong><?= $defender->getName() ?></strong>
                        
                        <?php if ($defender->getHealth() == 0) : ?>
                        <span class="action-state"><?= $defender->getName() ?> est mort</span>
                        <?php endif; ?>
                    </button>

                    <div class="action-content" id="action-content-<?= $size_attacker ?>" aria-labelledby="action-btn-<?= $size_attacker ?>" aria-expanded="false">
                        <div class="action-dammage">Coup porté : <strong><?= $attacker->getDammage() ?></strong></div>
                        <div class="action-characters">
                            <h3 class="site-subtitle">Héros</h3>
                            <?= $html_status_heros ?>
                        </div>
                        <div class="action-characters">
                            <h3 class="site-subtitle">Monstres</h3>
                            <?= $html_status_monsters ?>
                        </div>
                    </div>
                </div>