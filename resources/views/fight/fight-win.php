                <div class="victory">
                    <h3>Victoire !</h3>
                    <p>
                        Pippin se sentit curieusement attiré par le puits. Tandis que les autres déroulaient leurs couvertures et se confectionnaient des lits contre les murs de la salle, aussi loin que possible du trou dans le sol, il se glissa jusqu'au bord et pencha la tête sur l'orifice. Un air froid, montant des profondeurs invisibles, lui frappa le visage. Une impulsion soudaine lui fit saisir une pierre et la laisser tomber dans le vide. Il sentit son cœur battre bien des fois avant qu'il n'y eût aucun son. Puis, de loin en dessous, comme si la pierre était tombée dans l'eau profonde de quelque endroit caverneux, vint un plouf, très distant, mais amplifié et répété dans le creux du puits.<br>
                        - Qu'est-ce que c'est? cria Gandalf.<br>
                        Il fut soulagé quand Pippin avoua ce qu'il avait fait; mais il n'en était pas moins irrité, et Pippin vit ses yeux étinceler:<br>
                        - Stupide Touque! gronda le magicien. Ce voyage est sérieux, ce n'est pas une promenade de Hobbit. Jetez-vous dedans la prochaine fois, et ainsi vous ne gênerez plus personne. Et maintenant, restez tranquille!
                    </p>
                </div>