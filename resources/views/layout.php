<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=($title)? $title . ' | ' : '' ?>Un jeu Simplon Rodez</title>

    <link href="https://fonts.googleapis.com/css?family=Mina:400,700|Roboto:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="/css/global.css">

    <script src="/js/jquery.min.js" defer></script>
    <script src="/js/app.js" defer></script>
</head>
<body>
    <header class="site-header">
        <a href="/" class="header-logo"><img src="/img/simplonco.svg" alt="" width="50" height="50">Simplon War</a>
        <a href="/Team-show" class="btn">Recommencer le jeu</a>
    </header>
    <?=$content_html ?>
    <footer class="site-footer">
        <p class="footer-explain">Simplon war est un jeu proposé par la team #01 de Rodez</p>
        <p><a href="http://simplon.co">Simplon.co</a> | <a href="https://github.com/Simplon-Rodez/">Github Simplon Rodez</a></p>
    </footer>
</body>
</html>