<main class="site-main page">
    <h1 class="site-title">Créez vos équipes</h1>
    <form action="/Fight-show" method="post" class="site-section form">

        <?php foreach ($characters as $slug => $caracter) : ?>
        
        <div class="form-section">
            <h2 class="form-title">Les <?= $caracter['plurial'] ?></h2>

            <?php for ($i=1; $i<=3; $i++): ?>
            
            <div role="group" class="form-fieldset">
                <div role="legend" class="form-grouptitle"><?= $caracter['singular'] . ' ' . $i ?></div>

                <div class="form-group">
                    <label class="form-label" for="<?= $slug . '-' . $i ?>-name">Nom <abbr title="Champ obligatoire">*</abbr></label>
                    <input type="text" name="<?= $slug . '-' . $i ?>-name" id="<?= $slug . '-' . $i ?>-name" class="form-field" required>
                </div>
                
                <div class="form-group">
                    <label class="form-label" for="<?= $slug . '-' . $i ?>-type">Type <abbr title="Champ obligatoire">*</abbr></label>
                    <select name="<?= $slug . '-' . $i ?>-type" id="<?= $slug . '-' . $i ?>-type" class="form-field">

                        <?php foreach ($caracter['types'] as $slugType => $type) : ?>
                            <option value="<?= $slugType ?>"><?= $type ?></option>
                        <?php endforeach; ?>
                        
                    </select>
                </div>
            </div>
            <?php endfor; ?>
        </div>

        <?php endforeach; ?>
        
        <div class="form-submit">
            <p>Les champs munis d'une <abbr title="astérisque">*</abbr> sont obligatoires</p>
            <input type="submit" class="btn" value="Commencer le combat" name="submit">
        </div>
    </form>
</main>
