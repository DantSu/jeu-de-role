<?php

return [
    '^(/?)$' =>
        function (Controllers\Request $request, $action) {
            $request->setAction('Controllers\\Actions\\Home')->setMethod('show');
        },
    '/([a-z]+)-([a-z0-9_]+)' =>
        function(Controllers\Request $request, array $action) {
            $request->setAction('Controllers\\Actions\\'.$action[0])->setMethod($action[1]);
        }
    /*'/([a-z0-9_-]{2,3})' =>      ['action' => 'Controllers\Actions\Home',  'method' => 'show']
    '/teams' => ['action' => 'Controllers\Actions\Team',  'method' => 'show'],
    '/fight' => ['action' => 'Controllers\Actions\Fight', 'method' => 'show']*/
];